# contained-chrome

Chrome, running in container, inspiration:

- https://blog.jessfraz.com/post/docker-containers-on-the-desktop/#5-chrome
- https://satyamcs1999.medium.com/launching-google-chrome-on-docker-container-a7dc2ba2d5
- https://github.com/moby/moby/issues/8710#issuecomment-456062944
- https://github.com/jessfraz/dockerfiles

Also, inspiring, but not what I needed:
- https://github.com/moondev/gtk3-docker

**prerequisites**
`brew install xquartz socat`

Run this:
`socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"`

Then either of these:
`./run-chrome.sh` ( requires running `build-chrome.sh` first )
`./run-tor.sh` (not really chrome -- is firefox )
