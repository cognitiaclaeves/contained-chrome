FROM centos:8
LABEL maintainer "Satyam Singh"
RUN dnf update -y 
COPY google-chrome.repo /etc/yum.repos.d/
RUN yum install google-chrome-stable -y
RUN yum install mesa-libGL-devel -y
ENTRYPOINT ["google-chrome","--no-sandbox"]
